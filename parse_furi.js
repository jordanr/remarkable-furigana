function parse_ruby(state, check, checkIME) {
    
    var parenIME,
        marker,
        start;
    var pos = state.pos,
        max = state.posMax;
    
    //debugger;
    marker = state.src.charCodeAt(pos);

    if (marker === 0x28 /* ( */
        || (marker === 0xff08 /* ( IME */ && checkIME)) {
        parenIME = marker === 0xff08;
        pos++;
        if (check) {
            return true;
        }
    } else {
        return false;
    }

    // ( ruby )
    //  ^ skip this whitespace
    for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (code !== 0x20 && code !== 0x0A) { break; }
    }
    if (pos >= max) { return false; }

    // parse the ruby characters

    start = pos
    //debugger;
    for(; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (code === 0x20) { break; }
        if (code === 0x0A) { return false; }
        if (code === 0x29) /* ) */
            if (parenIME) {
                continue;
            } else {
                break;
            }
        if (code === 0xff09) /* ) IME */
            if (parenIME) {
                break;
            } else {
                continue;
            }
    }
    if (pos >= max) { return false };
    ruby = state.src.slice(start, pos);
    // ( ruby )
    //       ^ skip this whitespace
    for (; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (code !== 0x20 && code !== 0x0A) { break; }
    }
    // ( ruby )
    //        ^ -- we are hear and we don't know what's beyond
    //             so now we check and see if the last paren is really there
    if (pos > max) { return false; }
    code = state.src.charCodeAt(pos);

    //debugger;
    if (code === 0x29 /* ) */
        || code === 0xff09) { /* ) IME */
        if (code == 0x29 && parenIME) {
            return false;
        }
        if (code == 0xff09 && !parenIME) {
            return false;
        }
    } else {
        return false;
    }

    // yay we did it.
    // now we have to consume the last character.
    pos++;
    state.pos = pos;
    return ruby;
}

function parse_bracket(state, check) {
    var base,
        ruby,
        curlyIME;
    var oldPos = state.pos,
        max = state.posMax,
        pos = state.pos,
        start = state.pos,
        marker = state.src.charCodeAt(start);

    if (marker === 0x7b /* { */
        || marker === 0xff5b /* ime { */) {
        if (check) {
            state.pos++
            return true;
        }
        curlyIME = marker === 0xff5b;
        pos++;
        // { base }( ruby )
        //  ^ skip whitespace
        for (; pos < max; pos++) {
            code = state.src.charCodeAt(pos);
            if (code !== 0x20 && code !== 0x0A) { break; }
        }
        if (pos >= max) { return false; }
    } else {
        return false
    }
    
    //debugger;
    // now we're ready to parse the base
    start = pos;
    for(; pos < max; pos++) {
        code = state.src.charCodeAt(pos);
        if (code === 0x20) { break; }
        if (code === 0xff5d && curlyIME) { break; }
        if (code === 0x7d && ! curlyIME) { break; }
        if (code === 0x0A) { return false; }
         //     if (code === 0x28 /* ( */
        //|| code == 0xff08 /* ( IME */ 
        
    }
    if (pos >= max) {
        return false;
    }

    //debugger;
    base = state.src.slice(start, pos);
    
    
    // { base }( ruby )
    //       ^ skip whitespace
    for (; pos < max; pos++) {
        var code = state.src.charCodeAt(pos);
        if (code !== 0x20 && code !== 0x0A) { break; }
    }
    if (pos >= max) { return false; }
    

    marker = state.src.charCodeAt(pos);

    if (marker === 0x7D /* } */
        || marker === 0xff5d) /* } IME */ {
            if (marker === 0x7D && curlyIME) // no mixing syntaxes
                return false;
            pos++;
        }


    var oldPos = state.pos;
    state.pos = pos;

    ruby = parse_ruby(state, check, true);

    if (typeof(ruby) === 'boolean') {
        if (ruby) {
            state.pos++;
        } else {
            state.pos = oldPos;
        }
        return ruby;
    }

    //debugger;
    state.push({
        type: 'ruby',
        base: base,
        ruby: ruby,
        level: state.level
    });
    
    // console.log("I'm finished!");
    return true;
    
}


// taken from kuroshiro.js https://github.com/hexenq/kuroshiro.js/blob/master/src/kuroshiro.js#L27
/**
 * Check if given char is a kanji
 *
 * @param {string} ch Given char
 * @return {boolean} if given char is a kanji
 */
var isKanji = function(ch){
    ch = ch[0];
    return (ch >= '\u4e00' && ch <= '\u9fcf') ||
           (ch >= '\uf900' && ch <= '\ufaff') ||
           (ch >= '\u3400' && ch <= '\u4dbf') ||
           (ch == '\u3005') /* 々 */;
};

function parse_single(state, check) {

    var oldPos = state.pos;
    var base = state.src.charAt(state.pos);
    if (isKanji(base)) {
        if (check) {
            state.pos++
            return true;
        }
    } else {
        return false;
    }

    state.pos++;
    //debugger
    if (state.pos >= state.posMax) {
        state.pos = oldPos;
        return false;
    }
    var ruby = parse_ruby(state, check, true);

    //debugger;
    if (typeof(ruby) === 'boolean') {
        state.pos = oldPos;
        return ruby;
    }

    //debugger;
    state.push({
        type: 'ruby',
        base: base,
        ruby: ruby,
        level: state.level
    });
    
    // console.log("I'm finished!");
    return true;
}


function infer_kanji(state, check) {

    var oldPos = state.pos;
    var base = state.src.charAt(state.pos);
    if (isKanji(base)) {
        if (check) {
            state.pos++
            return true;
        }
    } else {
        return false;
    }

    state.pos++;

    for(; state.pos < state.posMax; state.pos++) {
      var code = state.src.charAt(state.pos);
      if (isKanji(code)) {
        base = base.concat(code);
      } else { break; }
    }

    //debugger
    if (state.pos >= state.posMax) {
        state.pos = oldPos;
        return false;
    }
    var ruby = parse_ruby(state, check, false);

    //debugger;
    if (typeof(ruby) === 'boolean') {
        state.pos = oldPos;
        return ruby;
    }

    //debugger;
    state.push({
        type: 'ruby',
        base: base,
        ruby: ruby,
        level: state.level
    });
    
    // console.log("I'm finished!");
    return true;
}

exports.brackets = parse_bracket;
exports.single = parse_single;
exports.infer_kanji = infer_kanji;

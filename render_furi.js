function render_token(token) {
    if (token['type'] !== 'ruby') {
        return "";
    } else {
        var base = token['base'];
        var ruby = token['ruby'];
        return "<ruby>" + base + "<rt>" + ruby + "</rt>" + "</ruby>";
    }
}
module.exports = function renderer(tokens, idx, options, env) {
    return render_token(tokens[idx]);
};

var parse = require("./parse_furi.js");
var renderer = require("./render_furi.js");
var text = require('./newtext.js');

module.exports = function remarkable_furigana(md, opts) {
    opts = opts || {};
    var brackets = opts['brackets'];
    if (typeof(brackets) == 'undefined') {
        brackets = true;
    }
    var single = opts['single'] || false;
    var infer_kanji = opts['infer_kanji'] || false;

    if (! (brackets || single)) {
        console.warn("remarkable_furi: All options turned off. Nothing registered");
        return;
    }
    md.inline.ruler.at('text', text, {}); // make our IME brackets terminal characters
    if (brackets) {
        md.inline.ruler.push('ruby', parse.brackets, {});
    }
    if (single) {
        md.inline.ruler.after('text', 'ruby', parse.single);
    }
    if (infer_kanji) {
        md.inline.ruler.after('text', 'ruby', parse.infer_kanji);
    }
    md.renderer.rules.ruby = renderer;
}

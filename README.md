# remarkable-furigana

## This plugin for remarkable allows you to add furigana characters to your markdown documents.


For example, writing something like this in your document: ``ご｛機嫌｝（きげん）よ``   will end up looking like   "ご<ruby>機嫌<rt>きげん</rt></ruby>よ".

What you should see, what your browser renders, is Chinese characters with smaller hiragana characters floating over them. What remarkable-furigana does under the hood is transform that easy bit of syntax into standard html ``<ruby>`` tags. The unrendered html output of the above looks like ご&lt;ruby&gt;機嫌&lt;rt&gt;きげん&lt;/rt&gt;&lt;/ruby&gt;よ.


### multiple syntaxes
This package supports two ways of writing markup in two different encoding styles.

#### single character syntax
You can enable a short single character syntax that's just ```字（じ）```.
The character before the parenthesis must be a Kanji character. 
To enable single syntax pass ```{'single': true}``` when you call ```.use```.

#### curly bracket syntax
Curly bracket syntax is enabled by default and it works by enclosing the kanji in curly brackets and the ruby in parenthesis. Like so {漢字}(かんじ).
You may also disable the curly bracket syntax by passing in ```'bracket': false``` with your options to md.use
#### infer kanji syntax
In this mode the parser will detect which characters are kanji and apply the preceding ruby to them. E.g. ```これは最高(さいこう)！``` will only apply the ruby to ```最高```.
To enable this syntax pass ```{'infer_kanji': true}``` when you call ```.use```.

See [remarkable](https://github.com/jonschlinkert/remarkable) for more documentation on initializing remarkable.

#### A note on "fat" vs. "skinny" brackets
If you're familiar with typing Japanese with an IME (Input Method Editor) then you've probably encountered "fat and "skinny" style of brackets. All you need to know about them here is that either will work.
The 'fat' brackets you type with your IME will work｛本｝（ほん） => <ruby>本<rt>ほん</rt></ruby>
The 'skinny' ascii brackets work as well  {本}(hon) => <ruby>本<rt>hon</rt></ruby>

It is important though that you consistently use the same kind of bracket.
If you use a 'fat bracket' and close it with a 'skinny one' like so "（ ) " the plugin will not work and you'll have a bad time.


<!-- ### try it out -->
<!-- To try it on your own machine run ```npm install``` then ``node example.js`` -->

<!-- To run tests use ```npm test``` -->

<!-- To run tests in your browser run ```npm run browser_test``` then navigate to where the nice http-server tells you to in whichever browser and hopefully you'll see your browser pass all of the tests. -->



<!-- If you are impatient and would like to try it in your browser right now on [runkit](https://npm.runkit.com/remarkable-furigana), then follow that url and copy and paste the code below into the site. -->
<!-- ```javascript -->
<!-- var remarkableFurigana = require("remarkable-furigana"); -->
<!-- var remarkable = require("remarkable"); -->

<!-- var md = new Remarkable(); -->

<!-- md.use(remarkableFurigana, {'single': true, 'bracket': true}); -->

<!-- console.log(md.render("利（り）にまどふは、すぐれて愚（おろ）かなる人（ひと）なり")); -->
<!-- console.log(md.render("{古文練習}(こぶんれんしゅう)")); -->

<!-- ``` -->







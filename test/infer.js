const { Remarkable } = require('remarkable');
var furi = require('../index.js');
var assert = require('assert');
var ut = require("../util.js");

var md = new Remarkable();
md.use(furi, {'infer_kanji': true });

describe('Infer Kanji', function() {
    var test1 = "重(かさ)なるのは人生(じんせい)のライン and レミリア最高(さいこう)！";
    it(test1 + " should infer kanji base characters", function() {

      assert.equal(
        md.render(test1),
        "<p><ruby>重<rt>かさ</rt></ruby>なるのは<ruby>人生<rt>じんせい</rt></ruby>のライン and レミリア<ruby>最高<rt>さいこう</rt></ruby>！</p>\n"
      );
    });
    var test2 = "random(word)";
    it(test2 + " shouldn't add ruby to the text", function() {

      assert.equal(
        md.render(test2),
        "<p>random(word)</p>\n"
      );
    });

    var test3 = "轔々(りんりん)";
    it(test3 + " should infer kanji + 々", function() {

      assert.equal(
        md.render(test3),
        "<p><ruby>轔々<rt>りんりん</rt></ruby></p>\n"
      );
    });
});
    


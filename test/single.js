const { Remarkable } = require('remarkable');
var furi = require('../index.js');
var assert = require('assert');
var ut = require("../util.js");

var md = new Remarkable();
md.use(furi, {'single': true, 'brackets': false});

describe('Single', function () {
    var test1 = '本（ほん）';
    it(test1 + " should produce 1 ruby token", function () {
        ut.assert_token_numbers(test1, md, {'ruby': 1});
    });

    var test2 = test1 + "[here](http:example.com) hi";
    it(test2 + " should produce 1 ruby and 1 link", function () {
        ut.assert_token_numbers(test2, md, {'ruby': 1, 'link_open': 1});
    });

    var test3 = '{base}(ruby)';
    it(test3 + " should have 0 ruby", function () {
        ut.assert_token_numbers(test3, md, {'ruby': 0});
    });
    var test4 = '日（に）本（ほん）語（ご）を書(か)いてみましょう'
    it(test4 + " should have 4 ruby", function () {
        ut.assert_token_numbers(test4, md, {'ruby': 4});
    });
    var test_double_kanji = '獲得'
    it(test_double_kanji + " should have 0 ruby and no undefined", function () {
        assert(md.render(test_double_kanji).search("undefined") == -1);
        ut.assert_token_numbers(test_double_kanji, md, {'ruby': 0})
    });
    var test_bold1 = '**' + test1 + '**'
    it(test_bold1 + " should render strong and ruby", function () {
        assert.strictEqual(
          md.render(test_bold1),
          '<p><strong><ruby>本<rt>ほん</rt></ruby></strong></p>\n'
        );
    });
    var test_bold2 = '**本**ほん';
    it(test_bold2 + " should render strong", function () {
        assert.strictEqual(
          md.render(test_bold2),
          '<p><strong>本</strong>ほん</p>\n'
        );
    });
    var test_bold3 = '**本**（ほん）';
    it(test_bold3 + " should render strong but not ruby", function () {
        assert.strictEqual(
          md.render(test_bold3),
          '<p><strong>本</strong>（ほん）</p>\n'
        );
        ut.assert_token_numbers(test_bold3, md, {'ruby': 0});
    });
    var test_bold4 = 'foo _bar_ foo **本**（ほん）';
    it(test_bold4 + " should render strong but not ruby", function () {
        assert.strictEqual(
          md.render(test_bold4),
          '<p>foo <em>bar</em> foo <strong>本</strong>（ほん）</p>\n'
        );
        ut.assert_token_numbers(test_bold4, md, {'ruby': 0});
    });
});
